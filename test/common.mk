samedir := $(abspath $(dir $(realpath $(lastword ${MAKEFILE_LIST}))))
# var source_dir is set before including this file
source_dir := $(abspath $(realpath ${source_dir}))

# hackhack: var default_rel_build_dir duplicated in check.sh
default_rel_build_dir := ../tmp
rel_build_dir ?= ${default_rel_build_dir}
build_dir := $(dir ${source_dir})/${rel_build_dir}/$(notdir ${source_dir})
$(shell mkdir -p ${build_dir})
build_dir := $(abspath $(realpath ${build_dir}))

${build_dir}/%.macro.json : ${source_dir}/%.macro
	build_dir=${build_dir} dakota_home=~/opt-stage/dakota ~/dakota-macro-system/bin/macro-system-parser.pl --output $@ $<

${build_dir}/%.macro.json : ${samedir}/%.macro
	build_dir=${build_dir} dakota_home=~/opt-stage/dakota ~/dakota-macro-system/bin/macro-system-parser.pl --output $@ $<

${build_dir}/%.dk.cc : ${source_dir}/%.dk | ${build_dir}/some.macro.json
	build_dir=${build_dir} dakota_home=~/opt-stage/dakota ~/dakota-macro-system/bin/macro-system.pl --input $| --output $@ $<

${build_dir}/%.dk.cc : ${source_dir}/%.dk | ${build_dir}/all.macro.json
	build_dir=${build_dir} dakota_home=~/opt-stage/dakota ~/dakota-macro-system/bin/macro-system.pl --input $| --output $@ $<

# disable built-in rules
.SUFFIXES :

.PRECIOUS : ${build_dir}/%.macro.json

.PHONY : all check clean

all : ${build_dir}/test.dk.cc

check : clean all
	${samedir}/check.sh ${source_dir} ${build_dir}

clean :
	rm -f ${source_dir}/*~ ${build_dir}/test.dk.cc ${build_dir}/some.macro.json ${build_dir}/all.macro.json
