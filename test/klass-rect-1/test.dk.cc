// -*- mode: c++; mode: dakota; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

KLASS-NS rect {
  /* interesting */
  struct slots-t {
    point-t     point;
    dimension-t dimension;
  }
;  METHOD init(object-t self) -> object-t {
    object-t o1 = $init($alloc(foo::klass()), NULL-KEYWORD);
    object-t o2 = $init($alloc(foo::klass()), __keyword:: _stuff_, fred, NULL-KEYWORD);
    return self;
  }
}
