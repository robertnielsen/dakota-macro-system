#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

function start() {
  local test=${1:-kw-args-use.dk}
  ~/dakota/stage-pre-built.sh && ./d-echo-transform $test && cat $test && echo '###' && cat tmp/$test.cc && wc -l $test tmp/$test.cc
  echo -n "whitespace $test:        " && grep -o ' ' $test        | wc -l
  echo -n "whitespace tmp/$test.cc: " && grep -o ' ' tmp/$test.cc | wc -l
  return
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
