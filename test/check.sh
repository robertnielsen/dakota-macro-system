#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

function start() {
  local source_dir=$1 build_dir=$2

  if test -e $source_dir/check.sh; then
    $source_dir/check.sh $source_dir $build_dir
  else
    if test ${silent:-0} -eq 0; then
      echo '###'
      cat $source_dir/test.dk
      echo '###'
      cat $build_dir/test.dk.cc
    fi
  fi
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
