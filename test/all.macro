// -*- mode: Dakota; c-basic-offset: 2; tab-width: 2; indent-tabs-mode: nil -*-

macro ?literal-pair {
  // 0,0
  {               ?literal-object : ?literal-object     }
  =>
  { pair::box ( { ?literal-object , ?literal-object } ) }

  // 0,1
  {               ?literal-object :    ?expr       }
  =>
  { pair::box ( { ?literal-object , #( ?expr ) } ) }

  // 1,0
  {                  ?expr   : ?literal-object     }
  =>
  { pair::box ( { #( ?expr ) , ?literal-object } ) }

  // 1,1
  {                  ?expr   :    ?expr       }
  =>
  { pair::box ( { #( ?expr ) , #( ?expr ) } ) }
} // ?literal-pair
main macro ?literal-object {
  { ?literal-expr }
  =>
  { ?literal-expr }

  { ?literal-table }
  =>
  { ?literal-table }

  { ?literal-set }
  =>
  { ?literal-set }

  {       ?symbol   }
  =>
  { box ( ?symbol ) }
} // ?literal-object
main macro ?literal-expr {
  { #( ?literal-pair ) }
  =>
  {    ?literal-pair   }

  {             #(             ?expr )   }
  =>
  { literal-box ?(substr ?1 1) ?(same-line ?expr ?-1) }
//{ literal-box (              ?expr )   }
} // ?literal-expr
main macro ?literal-sequence {
} // ?literal-sequence
main macro ?literal-table {
  { #{ : } }
  =>
  {  make \( ?(swap ?2 LITERAL-TABLE-KLASS) ?(swap ?-1 \) ) }
//{ make ( LITERAL-TABLE-KLASS ) }

  {                                                               #{             ?literal-pairs-in }     }
  =>
//{ make ( LITERAL-TABLE-KLASS , #objects : cast ( object-t [ ] ) {              ?literal-pairs-in }   ) }
  { make ( LITERAL-TABLE-KLASS , #objects : cast ( object-t [ ] ) ?(substr ?1 1) ?literal-pairs-in ?-1 ) }

  ?literal-pairs-in // aux-rule
  { ?literal-pair , ?... }
  =>
  { ?literal-pair , ?... }

  { ?literal-pair }
  =>
  { ?literal-pair , NULL-MEMBER }
} // ?literal-table
main macro ?literal-set {
  { #{ } }
  =>
  { make ( LITERAL-SET-KLASS ) }

  { #{ ?syms-in } }
  =>
//{ make ( LITERAL-SET-KLASS , #objects : cast ( object-t [ ] ) {              ?syms-in }   ) }
  { make ( LITERAL-SET-KLASS , #objects : cast ( object-t [ ] ) ?(substr ?1 1) ?syms-in ?-1 ) }

  ?syms-in // aux-rule
  { ?sym , ?... }
  =>
  { ?sym , ?... }

  { ?sym }
  =>
  { ?sym , NULL-MEMBER }

  ?sym // aux-rule
  { ?symbol }
  =>
  { box ( ?symbol ) }
} // ?literal-set
main macro ?include-stmt {
  { include ?dquote-str ; }
  =>
  { # include ?dquote-str }
} // ?include-stmt
main macro ?klass-or-trait-decl {
  { ?/klass|trait/ ?ident ; }
  =>
  { }
} // ?klass-or-trait-decl
main macro ?klass-defn {
  { klass    ?ident \{ }
  =>
  { KLASS-NS ?ident \{ }
}
main macro ?trait-defn {
  { trait    ?ident \{ }
  =>
  { TRAIT-NS ?ident \{ }
}
main macro ?superklass-decl {
  { superklass ?ident ; }
  =>
  { }
} // ?superklass-decl
main macro ?slots-defn {
  { ?slots ?block }
  =>
  { ?slots ?block ; }

  ?slots // aux-rule
  { slots }
  =>
  { struct slots-t }
} // ?slots-defn
main macro ?keyword-args-defn {
  { ?type ?ident : ?list-member }
  =>
  { ?type ?ident }
} // ?keyword-args-defn
main macro ?keyword-args-use {
  ?gfunc-name { ?/$init|$append/ } // eventually get from a generated per-project file

  { ?gfunc-name (  ?list-member ,  ?keyword-args )  }
  =>
  { ?gfunc-name (  ?list-member ,  ?keyword-args )  }

  { ?gfunc-name (  ?list-member                  )  }
  =>
  { ?gfunc-name (  ?list-member ,  NULL-KEYWORD  )  }

  ?keyword-args // aux-rule
  { ?keyword-expand : ?list-member ,  ?...         }
  =>
  { ?keyword-expand , ?list-member ?4 ?...         }

  { ?keyword-expand : ?list-member                 }
  =>
  { ?keyword-expand , ?list-member ,  NULL-KEYWORD }

  ?keyword-expand // aux-rule
  { ?keyword }
  =>
  { __keyword :: ?(concat _ ?(substr ?keyword 1) _) }
} // ?keyword-args-use
main macro ?box-symbol {
  { box ( ?symbol ) }
  =>
  { symbol::box ?2 __symbol :: ?(concat _ ?(substr ?symbol 1) _) ?-1 }
} // ?box-symbol
main macro ?method-alias {
  { method alias ?list }
  =>
  { method }
} // ?method-alias
main macro ?va-method-defn {
  { ?visibility method va ::          ?ident ?list -> ?type ?block   }
  =>
  { namespace va { ?visibility method ?ident ?list -> ?type ?block } }

  { method    va ::       ?ident ?list -> ?type ?block   }
  =>
  { namespace va { method ?ident ?list -> ?type ?block } }
} // ?va-method-defn
main macro ?export-method {
  { export method ?ident ?list -> ?type }
  =>
  { extern ?ident ?list -> ?type }
} // ?export-method
main macro ?method {
  { method ?ident ?list -> ?type }
  =>
  { METHOD ?ident ?list -> ?type }
} // ?method
main macro ?gfunc-or-method-call-super {
  { ?gfunc-ident \( ?super ?list-member-term }
  =>
  { ?gfunc-ident \( ?super ?list-member-term }

  { ?ident       \( ?super ?list-member-term }
  =>
  { ?ident       \( ?super ?list-member-term }

  ?super // aux-rule
  { super                      }
  =>
  { super ( { self , klass } ) }
} // ?gfunc-or-method-call-super
main macro ?slot-access {
  { self . ?ident }
  =>
  { unbox ( self ) . ?ident }
} // ?slot-access
main macro ?specific-box {
  ?type-name { ?/(.+?)-t/ }

  {               literal-box \( cast ( ?type-name ) }
  =>
  { ?type-name :: box         \(                     }
} // ?specific-box
main macro ?explicit-box-literal {
  { ?ident :: box (                                       { ?balenced-in } ) }
  =>
  { ?ident :: box ( cast ( ?(concat ?(strip ?ident) -t) ) { ?balenced-in } ) }
//{ ?ident :: box ( cast ( ?(concat         ?ident  -t) ) { ?balenced-in } ) } // bugbug: use ?ident twice without adjusting its ws

  { ?ident :: box (                                       ( ?balenced-in ) ) }
  =>
  { ?ident :: box ( cast ( ?(concat ?(strip ?ident) -t) ) ( ?balenced-in ) ) }
//{ ?ident :: box ( cast ( ?(concat         ?ident  -t) ) ( ?balenced-in ) ) } // bugbug: use ?ident twice without adjusting its ws
} // ?explicit-box-literal
// throw "..."
// throw #foo
// throw #"..." ;
// throw #[...] ;
// throw #{...} ;
// throw box(...) ;
// throw foo:box(...) ;
// throw make(...) ;
// throw klass ;
// throw self ;
main macro ?throw-make-or-box {
  ?object-creation-func { ?/make|box|literal-box/ }

  { throw                                   ?object-creation-func ( ?balenced-in )     }
  =>
  { throw   dkt-capture-current-exception ( ?object-creation-func ( ?balenced-in ) )   }

  { throw (                                 ?object-creation-func ( ?balenced-in ) )   }
  =>
  { throw ( dkt-capture-current-exception ( ?object-creation-func ( ?balenced-in ) ) ) }

//   { throw ?literal-collection }
//   =>
//   { throw dkt-capture-current-exception ( ?literal-collection ) }
//
//   { throw ( ?literal-collection ) }
//   =>
//   { throw ( dkt-capture-current-exception ( ?literal-collection ) ) }
} // ?throw-make-or-box
main macro ?make {
  { make            \( ?list-member   }
  =>
  { $init  ( $alloc ?2 ?list-member ) }
} // ?make
main macro ?export-enum {
  { export enum ?type-ident ?block }
  =>
  { }
} // ?export-enum
// ?(optional ?[ keys elements ] )
// ?(optional not )
// ?(optional va : )
// ?(not ?look-ahead xx yy )
// ?(not ?look-behind xx yy )

// if|while (e [not] in [keys|elements] tbl)
// if|while (e [not] in tbl)
main macro ?if-or-while-in-iterable { // optional 'not' and optional 'keys|elements'
  ?cond         { ?/if|when/ }
  ?sublist-func { ?/keys|elements/ }

  { ?cond (  ?ident in ?sublist-func   ?list-member )     }
  =>
  { ?cond ?2 ?ident in ?sublist-func ( ?list-member ) ?-1 }

  { ?cond ( ?ident in ?list-member ) }
  =>
  { ?cond ( in ( ?list-member , ?ident ) ) }
} // ?if-or-while-in-iterable
// for (object-t e [not] in [keys|elements] tbl)
// for (object-t e [not] in seq)
main macro ?for-in-iterable {
  ?sublist-func { ?/keys|elements/ }

  { for (   object-t ?ident in ?sublist-func   ?list-member   )   }
  =>
  { for ?2  object-t ?ident in ?sublist-func ( ?list-member ) ?-1 }

  { for (  object-t ?ident in                        ?list-member                                              )   }
  =>
  { for ?2 object-t _iterator_ = $forward-iterator ( ?list-member ) ; object-t ?ident = $next ( _iterator_ ) ; ?-1 }
} // ?for-in-iterable

// default values
// optional sequence of tokens (more than one)
// resolve concatenation and stringification
// comments
// how about include <...> ;

// only in template: ?(my-fuction some-pattern-var) to do some textual transformation (also passes in macro or macro/sub-macro name and rule number)

// #()   expr    NEVER empty
// #[]   sequence
// #{}   set
// #{:}  table

// ?{ident}-t ?ident = box({ ... })
// =>
// ?{ident}-t ?ident = ?{ident}:box({ ... })
// or
// ?type ?ident = box({ ... })
// =>
// ?type ?ident = box(cast(?type){ ... })

// foo:slots-t* slt = unbox(bar)
// =>
// foo:slots-t* slt = foo:unbox(bar)

// foo:slots-t& slt = *unbox(bar)
// =>
// foo:slots-t& slt = *foo:unbox(bar)

// foo-t* slt = unbox(bar)
// =>
// foo-t* slt = foo:unbox(bar)

// foo-t& slt = *unbox(bar)
// =>
// foo-t& slt = *foo:unbox(bar)

// from python
//
// with-file open(filename, "r") as fd { ?block-in }
// =>
// int-t fd = open(filename, "r") { ?block-in; close(fd) }
