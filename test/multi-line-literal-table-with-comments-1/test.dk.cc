  object-t o = $init($alloc(LITERAL-TABLE-KLASS), __keyword::_objects_, cast(object-t[]) {
    /* A */ pair::box(cast(pair-t){ symbol::box(__symbol::_a_), literal-box(x) }),
    /* B */ pair::box(cast(pair-t){ symbol::box(__symbol::_b_), literal-box(y) }),
    /* C */ pair::box(cast(pair-t){ symbol::box(__symbol::_c_), literal-box(z)
  }), NULL-MEMBER}, NULL-KEYWORD);
