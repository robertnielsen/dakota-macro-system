#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)
prefix=$(cd $samedir/.. && pwd)

dakota_source_dir=~/dakota
dakota_home=~/opt-stage/dakota

function cpu-core-count() { echo 4; }
function start() {
  local paths=( $@ )
  if test ${#paths[@]} -eq 0; then
    paths=( $(echo $dakota_source_dir/*/*.dk) )
  fi
  local build_dir=/tmp/$USER/macro-system
  rm -rf $build_dir
  mkdir -p $(dirname $build_dir)

  local default_macros_path=$build_dir/all.macro.json

  dakota_home=~/opt-stage/dakota $prefix/bin/macro-system-parser.pl --output $default_macros_path $samedir/all.macro

  # this allows xargs to run $proc (concurrent) processes each with $lines (or less) lines per process
  local procs=${procs:-$(cpu-core-count)}
  local lines=$(( ${#paths[@]} / $procs ))
  rm -f $build_dir/summary.txt
  { (printf "%s\n" ${paths[@]} | sort | dakota_home=$dakota_home DKT_MACROS_DEBUG=${DKT_MACROS_DEBUG:-0} summary=1 \
    xargs -L $lines -P $procs $prefix/bin/macro-system.pl --input $default_macros_path --output-dir $build_dir) } >> $build_dir/summary.txt

  local verbose=${verbose:-false}

  if $verbose; then
    build_dir=$build_dir $prefix/bin/macro-system-diff.sh ${paths[@]} > $build_dir/diff.txt
    cat $build_dir/diff.txt
    rm $build_dir/diff.txt
  fi

  cat $build_dir/summary.txt
  rm $build_dir/summary.txt
  echo "results in $build_dir/" 1>&2
  echo "elapsed: $(($SECONDS / 60))m$(($SECONDS % 60))s" 1>&2
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
