/*
overview
*/

KLASS-NS point
{

    // how do you like them apples?

  slots;
  struct
  slots-t{
    int-t x; // only whole numbers
    int-t y;
  }
;  slots int32-t (*)(void *, char *);
  slots int[2];
  slots uint32-t;
  /*
    method description here
  */

  method object-t init(object-t self, int-t x, int-t y)
  {
    self = $init(super({self,klass}),NULL-KEYWORD);
    self = $init(super({self,klass}),__keyword:: _other_, other-arg,NULL-KEYWORD);
    $init($alloc(klass),NULL-KEYWORD);
    $init($alloc(klass),__keyword:: _name_, fred,__keyword:: _length_, 4,NULL-KEYWORD)
    unbox(self).x
    unbox(self).y
    throw "hello world"
  //throw #foo-bar
    throw #"--out--"
  //for (object-t element in collection)
    for (object-t _iterator_=$forward-iterator( collection);object-t element =$next(_iterator_);)
      do-thing(element);

  //for (object-t element in collection) {
    for (object-t _iterator_=$forward-iterator( collection);object-t element =$next(_iterator_);) {
      do-thing(element);
    }
  }
}
