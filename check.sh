#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

export SHELLOPTS # opts set using 'set -o ...'
export BASHOPTS  # opts set using 'shopt ...'

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

function build-dir-from-source-dir() {
  local source_dir=$1 rel_build_dir=$2
  local build_dir; build_dir=$(dirname $source_dir)/$rel_build_dir/$(basename $source_dir)
  echo $build_dir
}
function abs-path() {
  local path=$1
  if test $path != /; then
    path=$(cd $(dirname $path) && pwd)/$(basename $path)
  fi
  echo $path
}
function start() {
  local dirs=( $@ )
  local rel_dir=test

  if test ${#dirs[@]} -eq 0; then
    local build_file; for build_file in $samedir/$rel_dir/*/build.mk; do
      dirs+=( $(dirname $build_file) )
    done
  fi
  local source_dirs=()

  local dir; for dir in ${dirs[@]}; do
    source_dirs+=( $(abs-path $dir) )
  done
  local rc
  local check_errs_count=0 check_errs=()
  local line_errs_count=0  line_errs=()
  local diff_errs_count=0  diff_errs=()

  local silent_opt=--silent
  if test ${no_silent:-0} -ne 0; then silent_opt=; fi

  local source_dir; for source_dir in ${source_dirs[@]}; do
    if test ${no_silent:-0} -eq 0; then
      : #echo "make ... --file $source_dir/build.mk check" 1>&2
    fi
    summary=${summary:-1} make $silent_opt --no-builtin-rules --no-builtin-variables --warn-undefined-variables --file $source_dir/build.mk check && rc=$? || rc=$?

    if test $rc -ne 0; then
      check_errs_count=$(( $check_errs_count + 1 ))
      check_errs+=( $rel_dir/$(basename $source_dir) )
    fi
    local default_rel_build_dir=../tmp # hackhack: var default_rel_build_dir duplicated in common.mk
    local build_dir; build_dir=$(build-dir-from-source-dir $source_dir ${rel_build_dir:-$default_rel_build_dir})
    if test $source_dir/test.dk.cc != $build_dir/test.dk.cc; then
      # --ignore-all-space same as -w
      diff --ignore-all-space $source_dir/test.dk.cc $build_dir/test.dk.cc && rc=$? || rc=$?

      if test $rc -ne 0; then
        diff_errs_count=$(( $diff_errs_count + 1 ))
        diff_errs+=( $rel_dir/$(basename $source_dir) )

        if ! test -e $source_dir/test.dk.cc; then
          echo "error: missing '$source_dir/test.dk.cc'" 1>&2
          echo "maybe use: cp $build_dir/test.dk.cc $source_dir/test.dk.cc" 1>&2
          echo "maybe use: git add $source_dir/test.dk.cc" 1>&2
        fi
      fi
    fi
    local in_lines;  in_lines=$(cat $source_dir/test.dk.cc | wc -l)
    local out_lines; out_lines=$(cat $build_dir/test.dk.cc | wc -l)

    if test $in_lines -ne $out_lines; then
      line_errs_count=$(( $line_errs_count + 1 ))
      line_errs+=( $rel_dir/$(basename $source_dir) )
    fi

  done
  if test $check_errs_count -ne 0; then
    echo "# check: failures=$check_errs_count" 1>&2
    printf "  %s\n" ${check_errs[@]} 1>&2
  fi
  if test $diff_errs_count -ne 0; then
    echo "# diff: failures=$diff_errs_count" 1>&2
    printf "  %s\n" ${diff_errs[@]} 1>&2
  fi
  if test $line_errs_count -ne 0; then
    echo "# line: failures=$line_errs_count" 1>&2
    printf "  %s\n" ${line_errs[@]} 1>&2
  fi
  if test $check_errs_count -ne 0 || test $diff_errs_count -ne 0 || test $line_errs_count -ne 0; then
    exit 1
  fi
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
