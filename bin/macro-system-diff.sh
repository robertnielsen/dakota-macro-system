#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#set -o xtrace && export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

function start() {
  for path in $@; do
    name=$(basename $path)
    if ! diff --brief $path $build_dir/$name.cc > /dev/null; then
      echo "diff $path $build_dir/$name.cc"
      diff $path $build_dir/$name.cc || true
      echo "wc -l $path $build_dir/$name.cc"
      wc -l $path
      wc -l $build_dir/$name.cc
    fi
  done
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  start "$@"
fi
