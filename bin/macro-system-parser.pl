#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

# Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

package dakota::macro_system;

use strict;
use warnings;
use sort 'stable';
use Cwd;

my $nl = "\n";

BEGIN {
  unshift @INC, "$ENV{'dakota_home'}/lib";
};

use dakota::util;
use dakota::sst;

use Carp;
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Useqq     = 1;
$Data::Dumper::Sortkeys  = 1;
$Data::Dumper::Indent    = 1;   # default = 2

use Getopt::Long qw(GetOptionsFromArray);
$Getopt::Long::ignorecase = 0;

my ($id,  $mid,  $bid,  $tid,
   $rid, $rmid, $rbid, $rtid) = &dakota::util::ident_regex();

# macro <id> {
#   <id> { ... } // alias
#   <id> { ... } // alias
#
#        { ... } => { ... } // main-ruleset
#        { ... } => { ... }
#
#   <id> { ... } => { ... } // aux-ruleset
#        { ... } => { ... }
#   <id> { ... } => { ... } // aux-ruleset
#        { ... } => { ... }
# }

sub _00 {
  my ($sst_cursor, $result) = @_;
  if (&sst_cursor::current_tkn_p($sst_cursor)) {
    my $start_tkn = &sst_cursor::current_tkn($sst_cursor);

    if ($start_tkn eq "main") {
      &sst_cursor::match($sst_cursor, "main");
    }
    &sst_cursor::match($sst_cursor, "macro");
    my $macro_name = &sst_cursor::match_re($sst_cursor, $id);

    # optionally match 'before' macro sequence: [ ident* ]

    &sst_cursor::match($sst_cursor, "{");
    if ($start_tkn eq "main") {
      &add_last($$result{'names'}, $macro_name);
    }
    $$result{'defn'}{$macro_name} =
      { 'aliases' => {}, 'rules' => [], 'aux-rules' => {} };
    my $name = undef;
    &_10($sst_cursor, $result, $macro_name, $name);
  }
}
sub _10 {
  my ($sst_cursor, $result, $macro_name, $name) = @_;

  for(&sst_cursor::current_tkn($sst_cursor)) {
    if (m/$id/) { # alias or aux-rule
      my $name = &sst_cursor::match_re($sst_cursor, $id);
      &_20($sst_cursor, $result, $macro_name, $name);
      last;
    }
    if (m/{/) { # rule or aux-rule
      #&sst_cursor::match($sst_cursor, "{");
      &_30($sst_cursor, $result, $macro_name, $name);
      last;
    }
    if (m/}/) { # macro end
      &sst_cursor::match($sst_cursor, "}");
      &_00($sst_cursor, $result);
      last;
    }
    die;
  }
} # _10
sub _20 {
  my ($sst_cursor, $result, $macro_name, $name) = @_;

  for(&sst_cursor::current_tkn($sst_cursor)) {
    if (m/{/) { # alias or aux-rule
      #&sst_cursor::match($sst_cursor, "{");
      &_30($sst_cursor, $result, $macro_name, $name);
      last;
    }
    die;
  }
}
sub _30 {
  my ($sst_cursor, $result, $macro_name, $name) = @_;

  my $rule = {};
  #&sst_cursor::match($sst_cursor, "{");
  $$rule{'pattern'} = &block($sst_cursor);
  #die "alias/rule-pattern may not be empty" if ! $$rule{'pattern'} || scalar @{$$rule{'pattern'}} == 0;
  #&sst_cursor::match($sst_cursor, "}");

  for (&sst_cursor::current_tkn($sst_cursor)) {
    if (m/=>/) {
      &sst_cursor::match($sst_cursor, "=>");
      #&sst_cursor::match($sst_cursor, "{");
      $$rule{'template'} = &block($sst_cursor);
      #&sst_cursor::match($sst_cursor, "}");

      if ($name) { # aux-rule
        $$result{'defn'}{$macro_name}{'aux-rules'}{$name} = [] if ! defined $$result{'defn'}{$macro_name}{'aux-rules'}{$name};
        &add_last($$result{'defn'}{$macro_name}{'aux-rules'}{$name}, $rule);
      } else { # rule
        push @{$$result{'defn'}{$macro_name}{'rules'}}, $rule;
      }
      last;
    }
    if (m/$id/ || m/{/) { # alias
      die "missing => or missing alias name" if ! $name;
      $$result{'defn'}{$macro_name}{'aliases'}{$name} = $$rule{'pattern'};
      $name = undef;
      last;
    }
  }
  &_10($sst_cursor, $result, $macro_name, $name);
}
# [ a, b,   ?(, c, d, ),    e, f ]
# =>
# [ a, b, [ ?(, c, d, ), ], e, f ]
#
# for all ?(...), ?[...], ?{...}
sub rewrite_rule_part {
  my ($rule_part, $balenced_info) = @_;
  my $rule_part_balenced_tbl = {
    '?(' => ')',
    '?{' => '}',
    '?[' => ']',
  };
  my $result = [];
  for (my $i = 0; $i < @$rule_part; $i++) {
    if ($$rule_part_balenced_tbl{$$rule_part[$i]}) {
      my $j = &seq::balenced($rule_part, $balenced_info, $i);
      die if $j - $i < 2;
      my $sub_rule_part = [ @$rule_part[$i + 1..$j - 1] ];
      $sub_rule_part = &rewrite_rule_part($sub_rule_part, $balenced_info); # recursive
      &add_last($result, [ $$rule_part[$i],
                           @$sub_rule_part,
                           $$rule_part[$j] ] );
      $i = $j;
    } else {
      &add_last($result, $$rule_part[$i]);
    }
  }
  return $result;
} # rewrite_rule_part
sub block {
  my ($sst_cursor) = @_;
  my ($open_token_index, $close_token_index) = &sst_cursor::balenced_range($sst_cursor);
  #my $result = &sst_cursor::slice($sst_cursor, $open_token_index, $close_token_index);
  my $result = [];
  &sst_cursor::match($sst_cursor, '{');
  for (my $i = $open_token_index; $i < $close_token_index - 1; $i++) {
    my $token = &sst_cursor::match_any($sst_cursor);
    &add_last($result, $token);
  }
  &sst_cursor::match($sst_cursor, '}');
  $result = &rewrite_rule_part($result, $$sst_cursor{'sst'}{'lang-info'}{'-balenced-'});
  return $result;
} # block
sub merge {
  my ($outputs) = @_;
  my $result = { 'names' => [], 'defn' => {} };
  foreach my $pair (@$outputs) {
    my $file =   $$pair[0];
    my $macros = $$pair[1];
    foreach my $name (keys %{$$macros{'defn'}}) {
      my $defn = $$macros{'defn'}{$name};

      if ($$result{'defn'}{$name}) {
        die "$file: macro '$name' already defined";
      }
      $$result{'defn'}{$name} = $defn;
    }
    foreach my $name (@{$$macros{'names'}}) {
      &add_last($$result{'names'}, $name);
    }
  }
  return $result;
} # merge
sub write_output {
  my ($cmd_info, $str, $file, $suffix) = @_;

  if ($$cmd_info{'opts'}{'output-dir'}) {
    my $bname = &basename($file);
    &_filestr_to_file($str, "$$cmd_info{'opts'}{'output-dir'}/$bname" . $suffix);
  } else {
    if (! $$cmd_info{'opts'}{'output'}) { $$cmd_info{'opts'}{'output'} = "/dev/stdout"; }
    &_filestr_to_file($str, $$cmd_info{'opts'}{'output'});
  }
} # write_output
sub write_merged_output {
  my ($cmd_info, $outputs) = @_;
  return if $$cmd_info{'opts'}{'output-dir'} && ! $$cmd_info{'opts'}{'output'};
  if (! $$cmd_info{'opts'}{'output'}) { $$cmd_info{'opts'}{'output'} = "/dev/stdout"; }
  my $str = &Dumper(&merge($outputs));
  &_filestr_to_file($str, $$cmd_info{'opts'}{'output'});
} # write_merged_output

# this is hack to work around the fact that the sst requires balenced bracketing
# so we have to escape unbalenced brackets {} () [] in our <>.macro files
sub unescape_strs_in_seq {
  my ($seq) = @_;
  for (my $i = 0; $i < @$seq; $i++) {
    if (ref($$seq[$i]) eq 'ARRAY') {
      &unescape_strs_in_seq($$seq[$i]);
    } else {
      $$seq[$i] =~ s/^\\(\{|\}|\(|\)|\[|\])$/$1/;
    }
  }
} # unescape_strs_in_seq
sub unescape_brackets { # hackhack
  my ($macros) = @_;
  while (my ($name, $defn) = each (%{$$macros{'defn'}})) {
    while (my ($name, $seq) = each (%{$$defn{'aliases'}})) {
      &unescape_strs_in_seq($seq);
    }
    my $aux_rules = [];
    while (my ($name, $aux_rule) = each (%{$$defn{'aux-rules'}})) {
      &add_last($aux_rules, @$aux_rule);
    }
    foreach my $rule (@{$$defn{'rules'}}, @$aux_rules) {
      for my $seq ($$rule{'pattern'}, $$rule{'template'}) {
        &unescape_strs_in_seq($seq);
      }
    }
  }
} # unescape_brackets
sub cmd_info_from_argv {
  my ($argv) = @_;
  my $cmd_info = { 'opts' => {} };
  &GetOptionsFromArray($argv,
                       $$cmd_info{'opts'},
                       'output=s',
                       'output-dir=s',
                     );
  $$cmd_info{'args'} = [ @$argv ]; # add what is left after getopts processing
  return $cmd_info;
} # cmd_info_from_argv
sub validate_cmd_line {
  my ($cmd_info) = @_;
  my $usage = "usage: macro-system-parser [--output <>] [--output-dir <>] <file> [<file> ...]";
  die $usage if scalar @{$$cmd_info{'args'}} == 0;
} # validate_cmd_line
sub start {
  my ($argv) = @_;
  my $cmd_info = &cmd_info_from_argv(\@ARGV);
  &validate_cmd_line($cmd_info);
  my $lang_info = &macro_lang_info();
  my $outputs = [];

  foreach my $file (@{$$cmd_info{'args'}}) {
    my $filestr = &filestr_from_file($file);

    my $sst = &sst::make($lang_info, $filestr, $file);
    my $sst_cursor = &sst_cursor::make($sst);
    my $macros = { 'names' => [], 'defn' => {} };
    &_00($sst_cursor, $macros);
    &unescape_brackets($macros); # hackhack
    if ($ENV{'DKT_MACRO_PARSER_DUMP'}) {
      print STDERR $file . $nl;
      print STDERR &Dumper($macros) . $nl;
    }
    &add_last($outputs, [ $file, $macros ]);
    &write_output($cmd_info, &Dumper($macros), $file, '.json');
  }
  &write_merged_output($cmd_info, $outputs);
} # start
unless (caller) {
  &start(\@ARGV);
}
1;
