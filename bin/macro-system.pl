#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

# Copyright (C) 2007 - 2022 Robert Nielsen <robert@dakota.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

package dakota::macro_system;

use strict;
use warnings;
use sort 'stable';
use Cwd;

my $nl = "\n";

BEGIN {
  unshift @INC, "$ENV{'dakota_home'}/lib";
};

use dakota::util;
use dakota::sst;

use Carp;
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Useqq     = 1;
$Data::Dumper::Sortkeys  = 1;
$Data::Dumper::Indent    = 1;   # default = 2

use Getopt::Long qw(GetOptionsFromArray);
$Getopt::Long::ignorecase = 0;

my ($id,  $mid,  $bid,  $tid,
   $rid, $rmid, $rbid, $rtid) = &dakota::util::ident_regex();

my $pattern_var_re = qr/\?.+/;

# token:     { 'tkn' => "", ... }
# token_i:   $$sst{'tokens'}[<token_i>]
# frag:      [ <token> ... ]
# frag_pair: (<token_i>, <lhs_frag>)

# alias:    <?ident> { pattern }
# rule:              { pattern } => { template }
# aux-rule: <?ident> { pattern } => { template }
#
# lhs: list of frags matching  pattern
# rhs: list of frags replacing template

# flatten() is used to convert a list of frags to a single frag
#   $lhs_frag = &flatten($lhs);

# pattern-variable constraints are required to have the following API:
# xxx ( $sst, $sst_i ) -> $last_i # aka: token_i
my $constraints = {
  '?balenced' =>         \&pattern_var_balenced,
  '?balenced-in' =>      \&pattern_var_balenced_in,
  '?block' =>            \&pattern_var_block,
  '?expr' =>             \&pattern_var_expr,
  '?list' =>             \&pattern_var_list,
  '?list-member' =>      \&pattern_var_list_member,
  '?list-member-term' => \&pattern_var_list_member_term, # move to a language specific macro
  '?literal-pair-in' =>  \&pattern_var_literal_pair_in,
  '?literal-table-in' => \&pattern_var_literal_table_in,
  '?type' =>             \&pattern_var_type,
};
# pattern-func constraints are required to have the following API:
# xxx ( $sst, $sst_i, ... ) -> ( $last_i, $lhs_frag ) # aka: frag_pair
my $pattern_funcs = {
  '?(' => {
    '=~' => \&pattern_func_regex_eq,
    '!~' => \&pattern_func_regex_ne,
  },
  '?{' => {
  },
  '?[' => {
  },
};
# template-funcs are required to have the following API:
# xxx ( ... ) -> $rhs_frag # aka: frag
my $template_funcs = {
  '?(' => {
    'concat' =>    \&template_func_concat,
    'substr' =>    \&template_func_substr,
    'swap' =>      \&template_func_swap,
    'uppercase' => \&template_func_uppercase,
    'lowercase' => \&template_func_lowercase,
    'same-line' => \&template_func_same_line,
    'strip' =>     \&template_func_strip,
    'print' =>     \&template_func_println,
    'println' =>   \&template_func_println,
  },
  '?{' => {
  },
  '?[' => {
  },
};

my $debug;
my $gbl_match_count = 0;
my $balenced_info;

sub dumpref {
  my ($str, $ref) = @_;
  return if $ENV{'disable_dumpref'};

  if ($str && ! defined $ref) {
    $ref = $str;
    $str = undef;
  }
  if ($str) {
    print STDERR $str;
  }
  print STDERR &Dumper($ref) . $nl;
} # dumpref
sub frags_remove_keys {
  my ($frags, $keys, $label) = @_;
  foreach my $frag (@$frags) {
    foreach my $token (@$frag) {
      foreach my $key (@$keys) {
        if (0) {
          if (exists $$token{$key}) {
            if ($label) {
              &dumpref("$label: ", $token);
            } else {
              &dumpref($token);
            }
          }
        }
        delete $$token{$key};
      }
    }
  }
} # frags_remove_keys
sub pattern_var_list_member_term { # move to a language specific macro
  my ($sst, $sst_i) = @_;
  my $tkn = &sst::at($sst, $sst_i);
  my $last_i = -1;

  if ($$sst{'lang-info'}{'balenced'}{'list'}{'sep'}{$tkn} || $$sst{'lang-info'}{'balenced'}{'list'}{'close'}{$tkn}) {
    $last_i = $sst_i;
  }
  return $last_i;
} # pattern_var_list_member_term
sub pattern_var_list_member {
  my ($sst, $sst_i) = @_;
  return -1 if &pattern_var_list_member_term($sst, $sst_i) != -1;
  my $j = 0;
  my $is_bracketed = 0;
  my $num_tokens = scalar @{$$sst{'tokens'}};

  while ($num_tokens > $sst_i + $j) {
    if (!$is_bracketed) {
      if (&pattern_var_list_member_term($sst, $sst_i + $j) != -1) {
        return $sst_i + $j - 1;
      }
    }
    my $tkn = &sst::at($sst, $sst_i + $j);

    if (exists $$sst{'lang-info'}{'-balenced-'}{'open'}{$tkn}) {
      $is_bracketed++;
    } elsif (exists $$sst{'lang-info'}{'-balenced-'}{'close'}{$tkn} && $is_bracketed) {
      $is_bracketed--;
    }
    $j++;
  }
  return -1;
} # pattern_var_list_member
sub pattern_var_literal_pair_in { # ?expr : ?expr
  my ($sst, $sst_i) = @_;
  my $num_tokens = scalar @{$$sst{'tokens'}};
  return -1 if $num_tokens < $sst_i + 3; # shall be 3 or more tokens
  my $last_i = &pattern_var_expr($sst, $sst_i, 'expr'); # first must be an expr (very broad)
  return -1 if $last_i == -1;
  my $sep_str = &sst::at($sst, $last_i + 1);
  return -1 if ! $$sst{'lang-info'}{'balenced'}{'literal-pair'}{'sep'}{$sep_str}; # this token shall be the ...{'sep'}...
  return &pattern_var_expr($sst, $last_i + 2, 'expr'); # last must be an expr (very broad)
} # pattern_var_literal_pair_in
sub pattern_var_literal_table_in {
  my ($sst, $sst_i) = @_;
  my $last_i = -1;
  my $i = $sst_i;
  my $num_tokens = scalar @{$$sst{'tokens'}};

  while ($num_tokens > $i) {
    $i = &pattern_var_literal_pair_in($sst, $i, 'literal-pair-in');
    return $last_i if $i == -1;
    $last_i = $i++;
    my $sep_str = &sst::at($sst, $i);
    return $last_i if ! $$sst{'lang-info'}{'balenced'}{'list'}{'sep'}{$sep_str};
    $i++;
  }
  return -1;
} # pattern_var_literal_table_in
sub pattern_var_expr {
  my ($sst, $sst_i) = @_;
  my $tkn = &sst::at($sst, $sst_i);
  return -1 if $$sst{'lang-info'}{'-balenced-'}{'sep'}{$tkn} || $$sst{'lang-info'}{'-balenced-'}{'close'}{$tkn};
  my $j = 0;
  my $is_bracketed = 0;
  my $num_tokens = scalar @{$$sst{'tokens'}};

  while ($num_tokens > $sst_i + $j) {
    $tkn = &sst::at($sst, $sst_i + $j);

    if (!$is_bracketed) {
      if ($$sst{'lang-info'}{'-balenced-'}{'sep'}{$tkn} || $$sst{'lang-info'}{'-balenced-'}{'close'}{$tkn}) {
        return $sst_i + $j - 1;
      }
    }
    if (exists $$sst{'lang-info'}{'-balenced-'}{'open'}{$tkn}) {
      $is_bracketed++;
    } elsif (exists $$sst{'lang-info'}{'-balenced-'}{'close'}{$tkn} && $is_bracketed) {
      $is_bracketed--;
    }
    $j++;
  }
  return -1;
} # pattern_var_expr
# this is very incomplete
sub pattern_var_type {
  my ($sst, $sst_i) = @_;
  my $tkn = &sst::at($sst, $sst_i);
  my $last_i = -1;

  my $type_tkns = { '*' => 1,
                    '&' => 1,
                    'const' =>  1,
                    'volatile' => 1 };

  if ($tkn =~ /^$$sst{'lang-info'}{'constraint-regex'}{'?type-ident'}$/) {
    my $j = 0;

    while ('*' eq &sst::at($sst, $sst_i + $j + 1)) {
      $j++;
    }
    $last_i = $sst_i + $j;
  }
  return $last_i;
} # pattern_var_type
sub pattern_var_block {
  my ($sst, $open_token_i) = @_;
  my $open_tkn = &sst::at($sst, $open_token_i);
  my $bracket = 'block';
  return -1 if ! $$sst{'lang-info'}{'balenced'}{$bracket}{'open'}{$open_tkn}; # 'block'
  return &pattern_var_balenced($sst, $open_token_i);
} # pattern_var_block
sub pattern_var_list {
  my ($sst, $open_token_i) = @_;
  my $open_tkn = &sst::at($sst, $open_token_i);
  my $bracket = 'list';
  return -1 if ! $$sst{'lang-info'}{'balenced'}{$bracket}{'open'}{$open_tkn}; # 'list'
  return &pattern_var_balenced($sst, $open_token_i);
} # pattern_var_list
sub pattern_var_balenced {
  my ($sst, $open_token_i) = @_;
  return &sst::balenced($sst, $open_token_i);
} # pattern_var_balenced
sub pattern_var_balenced_in {
  my ($sst, $sst_i) = @_;
  return &sst::balenced_in($sst, $sst_i);
} # pattern_var_balenced_in
sub literal { # not a pattern-variable constraint (because its a pattern-literal)
  my ($sst, $sst_i, $literal) = @_;
  die "invalid args" if ! $sst || ! $literal || $sst_i < 0;
  my $tkn = &sst::at($sst, $sst_i);
  my ($last_i, $lhs_frag) = (-1, undef);

  if ($tkn eq $literal) {
    ($last_i, $lhs_frag) = &frag_pair_with_tkn($sst, $sst_i, $tkn)
  }
  return ($last_i, $lhs_frag); # frag_pair
} # literal
sub pattern_func_regex_eq { # pattern-func constraint
  my ($sst, $sst_i, $regex) = @_;
  die "invalid args" if ! $sst || ! $regex || $sst_i < 0;
  $regex = &strip_leading_trailing_slashes($regex);
  $regex = &escape_perl_special($regex);
  my $tkn = &sst::at($sst, $sst_i);
  my ($last_i, $lhs_frag) = (-1, undef);

  if ($tkn =~ /$regex/) { # eq
    if (defined $1) { $tkn = $1; }
    ($last_i, $lhs_frag) = &frag_pair_with_tkn($sst, $sst_i, $tkn)
  }
  return ($last_i, $lhs_frag); # frag_pair
} # pattern_func_regex_eq
sub pattern_func_regex_ne { # pattern-func constraint
  my ($sst, $sst_i, $regex) = @_;
  die "invalid args" if ! $sst || ! $regex || $sst_i < 0;
  $regex = &strip_leading_trailing_slashes($regex);
  $regex = &escape_perl_special($regex);
  my $tkn = &sst::at($sst, $sst_i);
  my ($last_i, $lhs_frag) = (-1, undef);

  if ($tkn !~ /$regex/) { # ne
    ($last_i, $lhs_frag) = &frag_pair_with_tkn($sst, $sst_i, $tkn)
  }
  return ($last_i, $lhs_frag); # frag_pair
} # pattern_func_regex_ne
sub template_func_concat {
  my $frags = \@_;
  my $frag = &flatten($frags);
  my $lines = [];
  my $rhs_token = { 'tkn' => '' };
  my $rhs_frag = [];
  foreach my $token (@$frag) {
    $$rhs_token{'leading-ws'} .=  $$token{'leading-ws'}  if $$token{'leading-ws'};
    $$rhs_token{'tkn'} .=         $$token{'tkn'};
    $$rhs_token{'trailing-ws'} .= $$token{'trailing-ws'} if $$token{'trailing-ws'};
    &add_last($lines, $$token{'line'}) if defined $$token{'line'};

    # hackhack: this is incomplete and wrong when the frags have more than a single token with 'lhs-i'
    $$rhs_token{'lhs-i'} = $$token{'lhs-i'} if $$token{'lhs-i'} && ! $$rhs_token{'lhs-i'};
  }
  if (scalar @$lines) {
    my $line = (sort @$lines)[0];
    $$rhs_token{'line'} = $line;
  }
  &add_last($rhs_frag, $rhs_token);
  return $rhs_frag; # contract: len of rhs_frag is 1
} # template_func_concat
sub template_func_uppercase {
  my $frags = \@_;
  my $frag = &flatten($frags);
  my $rhs_frag = [];
  foreach my $token (@$frag) {
    my $rhs_token;
    %$rhs_token = %$token;
    $$rhs_token{'tkn'} = uc $$rhs_token{'tkn'};
    &add_last($rhs_frag, $rhs_token);
  }
  return $rhs_frag;
} # template_func_uppercase
sub template_func_lowercase {
  my $frags = \@_;
  my $frag = &flatten($frags);
  my $rhs_frag = [];
  foreach my $token (@$frag) {
    my $rhs_token;
    %$rhs_token = %$token;
    $$rhs_token{'tkn'} = lc $$rhs_token{'tkn'};
    &add_last($rhs_frag, $rhs_token);
  }
  return $rhs_frag;
} # template_func_lowercase
sub template_func_same_line {
  my $rhs_frags = \@_;
  my $rhs_frag = &flatten($rhs_frags);
  $rhs_frag = &adjust_trailing_ws($rhs_frag);
  return $rhs_frag;
} # template_func_same_line
sub template_func_strip {
  my $frags = \@_;
  my $frag = &flatten($frags);
  my $rhs_frag = [];
  foreach my $token (@$frag) {
    my $rhs_token = { 'tkn' => $$token{'tkn'} };
    &add_last($rhs_frag, $rhs_token);
  }
  return $rhs_frag;
} # template_func_strip
sub template_func_substr {
  my ($frag, $i_frag, $len_frag) = @_;
  my $i = $$i_frag[0]{'tkn'};
  my $len;
  $len = $$len_frag[0]{'tkn'} if defined $len_frag;
  my $rhs_frag = [];
  foreach my $token (@$frag) {
    $len = length($$token{'tkn'}) if ! defined $len_frag;
    my $rhs_token;
    %$rhs_token = %$token;
    $$rhs_token{'tkn'} = substr($$token{'tkn'}, $i, $len);
    &add_last($rhs_frag, $rhs_token);
  }
  return $rhs_frag;
} # template_func_substr
# currently on works with a single token (both)
# this should work for an arbitrary length frags (both)
sub template_func_swap {
  my ($lhs_frag, $rhs_frag) = @_;
  my $lhs_token = $$lhs_frag[0];
  my $rhs_token;
  %$rhs_token = %$lhs_token;
  $$rhs_token{'tkn'} = $$rhs_frag[0]{'tkn'};
  return [ $rhs_token ];
} # template_func_swap
sub template_func_print {
  my $frags = \@_;
  my $frag = &flatten($frags);
  my $d = '';
  my $ln = '';
  my $rhs_frag = [];
  foreach my $token (@$frag) {
    $ln .= $d . $$token{'tkn'};
    $d = ' ';
  }
  print "$ln";
  return $frag;
} # template_func_print
sub template_func_println {
  my $frag = &template_func_print(@_);
  print $nl;
  return $frag;
} # template_func_println
sub copy_token {
  my ($sst, $sst_i, $tkn) = @_;
  my $token;
  %$token = %{$$sst{'tokens'}[$sst_i]};
  if (defined $tkn) {
    $$token{'tkn'} = $tkn;
  }
  return $token;
} # copy_token
sub frag_pair_with_tkn {
  my ($sst, $sst_i, $tkn) = @_;
  my $token = &copy_token($sst, $sst_i, $tkn);
  my ($last_i, $lhs_frag) = ($sst_i, [ $token ]);
  return ($last_i, $lhs_frag); # frag_pair
} # frag_pair_with_tkn
sub strip_leading_trailing_slashes {
  my ($regex) = @_;
  $regex =~ s~^/(.+?)/$~$1~;
  return $regex;
} # strip_leading_trailing_slashes
sub escape_perl_special {
  my ($regex) = @_;
  $regex =~ s/(\$|\@|\$%)/\\$1/; # escape those special to perl, but not to its regex syntax (in matching context)
  return $regex;
} # escape_perl_special
sub assert {
  my ($result, $msg) = @_;

  if ($msg) {
    die $msg if $result != 0;
  } else {
    die if $result != 0;
  }
} # assert
sub dump_rule_match {
  my ($macro_name, $sst, $sst_i, $last_i, $lhs, $rhs, $pattern, $template) = @_;
  if ($debug >= 2 || ($last_i != -1 && $debug >= 1)) {
    # could use xhs_dump() to tighten up $lhs and $rhs
    print STDERR
      "  {" . $nl .
      "    \"macro\" =>    \"$macro_name\"," . $nl .
      "    \"range\" =>     " . &Dumper([$sst_i, $last_i]) . "," . $nl .
      "    \"pattern\" =>   " . &Dumper($pattern)  . "," . $nl .
      "    \"template\" =>  " . &Dumper($template) . "," . $nl .
      "    \"lhs\" =>       " . &Dumper($lhs)      . "," . $nl .
      "    \"rhs\" =>       " . &Dumper($rhs)      . "," . $nl .
      "  }," . $nl;
  }
} # dump_rule_match
sub dump_rules_match_and_replace {
  my ($macro_name, $sst, $sst_i, $last_i, $lhs, $rhs) = @_;
  if ($debug >= 2 || ($last_i != -1 && $debug >= 1)) {
    # could use xhs_dump() to tighten up $lhs and $rhs
    print STDERR
      "  {" . $nl .
      "    \"macro\" =>    \"$macro_name\"," . $nl .
      "    \"range\" =>     " . &Dumper([$sst_i, $last_i]) . "," . $nl .
      "    \"lhs\" =>       " . &Dumper($lhs) . "," . $nl .
      "    \"rhs\" =>       " . &Dumper($rhs) . "," . $nl .
      "  }," . $nl;
  }
} # dump_rules_match_and_replace
sub xhs_dump {
  my ($rhs) = @_;
  my $delim1 = '';
  my $delim2 = '';
  my $str = '';
  $str .= '[';

  foreach my $seq (@$rhs) {
    $delim2 = '';
    $str .= $delim1 . '[';
    $delim1 = ',';

    foreach my $token (@$seq) {
      $str .= $delim2 . '"' . $$token{'tkn'} . '"';
      $delim2 = ',';
    }
    $str .= ']';
  }
  $str .= ']';
  return $str;
} # xhs_dump
sub tighten {
  my ($filestr) = @_;

  # remove whitespace
  $filestr =~ s/\s*\[\s*/[/g;
  $filestr =~ s/\s*\]\s*/]/g;

  $filestr =~ s/\s*\(\s*/(/g;
  $filestr =~ s/\s*\)\s*/)/g;

  $filestr =~ s/\s*;/;/g;
  $filestr =~ s/\s*:\s*/:/g;

  # add whitespace
  $filestr =~ s/([^:]):([^:])/$1: $2/g;
  $filestr =~ s/\s*,\s*/, /g;

  $filestr =~ s/\s*{\s*/{ /g;
  $filestr =~ s/\s*}\s*/ }/g;

  return $filestr; # may or may not have trailing newline
} # tighten
sub write_output {
  my ($cmd_info, $str, $file, $suffix) = @_;

  if ($$cmd_info{'opts'}{'output-dir'}) {
    my $bname = &basename($file);
    &_filestr_to_file($str, "$$cmd_info{'opts'}{'output-dir'}/$bname" . $suffix);
  } else {
    if (! $$cmd_info{'opts'}{'output'}) { $$cmd_info{'opts'}{'output'} = "/dev/stdout"; }
    &_filestr_to_file($str, $$cmd_info{'opts'}{'output'});
  }
} # write_output
sub add_constraint_regexes {
  my ($constraints, $constraint_names) = @_;
  foreach my $constraint_name (@$constraint_names) {
    die "constraint '$constraint_name' already exists" if defined $$constraints{$constraint_name};

    my $func = sub {
      my ($sst, $sst_i) = @_;
      my $tkn = &sst::at($sst, $sst_i);
      die "" if ! defined $tkn;
      my $last_i = -1;

      if ($tkn =~ /^$$sst{'lang-info'}{'constraint-regex'}{$constraint_name}$/) {
        $last_i = $sst_i;
      }
      return $last_i;
    };
    $$constraints{$constraint_name} = $func;
  }
} # add_constraint_regexes
sub cmd_info_from_argv {
  my ($argv) = @_;
  my $cmd_info = { 'opts' => { 'input' => [] } };
  &GetOptionsFromArray($argv,
                       $$cmd_info{'opts'},
                       'summary',
                       'input=s',
                       'output=s',
                       'output-dir=s',
                     );
  $$cmd_info{'args'} = [ @$argv ]; # add what is left after getopts processing
  return $cmd_info;
} # cmd_info_from_argv
sub validate_cmd_line {
  my ($cmd_info) = @_;
  my $usage = "usage: macro-system --summary --input <> [--input <>] [--output <>] [--output-dir <>] <file> [<file> ...]";
  die $usage if scalar @{$$cmd_info{'opts'}{'input'}} == 0;
  die $usage if scalar @{$$cmd_info{'args'}}          == 0;
} # validate_cmd_line
sub start {
  my ($argv) = @_;
  my $cmd_info = &cmd_info_from_argv(\@ARGV);
  &validate_cmd_line($cmd_info);
  my $all_macros = [];

  foreach my $in_path (@{$$cmd_info{'opts'}{'input'}}) {
    my $input = do $in_path or die "'do $in_path' failed: $!" . $nl;
    $$input{'self'} = $in_path;
    &add_last($all_macros, $input);
  }
  my $lang_info = &dakota::sst::macro_lang_info();
  &add_constraint_regexes($constraints, [keys %{$$lang_info{'constraint-regex'}}]);

  foreach my $macros (@$all_macros) {
    foreach my $macro_name (keys %{$$macros{'defn'}}) {
      die "constraint and macro have same name: '$macro_name'" if defined $$constraints{$macro_name};
    }
  }
  my $changes = { 'files' => {} };
  my $output_dir = $$cmd_info{'opts'}{'output-dir'};
  $output_dir = $ENV{'build_dir'} if ! $output_dir;
  $Data::Dumper::Indent = 0; # redundant, but added for clarity
  my $num_macros = 0;
  my $num_tokens = 0;

  foreach my $file (@$argv) {
    my $filestr = &filestr_from_file($file);

    if (! $ENV{'silent'}) {
      if ($ENV{'verbose'}) {
        print STDERR $filestr;

        if ($filestr !~ m/\n$/) {
          print STDERR $nl;
        }
      } else {
        print STDERR $file . $nl;
      }
    }
    my $sst = &sst::make($lang_info, $filestr, $file);
    $balenced_info = $$sst{'lang-info'}{'-balenced-'};
    die "" if ! $balenced_info;
    $num_tokens += @{$$sst{'tokens'}};
    foreach my $macros (@$all_macros) {
      $num_macros += scalar keys %$macros;
      &macros_expand($sst, $macros);
    }
    #$$changes{'file'}{$file} = &sst::change_report($sst);
    $$changes{'files'}{$file} = $$sst{'changes'};
    &write_output($cmd_info, &sst::filestr($sst), $file, $$lang_info{'output-file-suffix'});
  }
  $$changes{'num-files'} = scalar keys %{$$changes{'files'}};
  $$changes{'num-total-changes'} = 0;
  my $lines = [];

  while (my ($file, $file_info) = each (%{$$changes{'files'}})) {
    $$changes{'files'}{$file}{'num-changes'} = 0;

    while (my ($macro, $count) = each (%{$$file_info{'macros'}})) {
      &add_last($lines, "$file : $macro : $count" . $nl);
      $$changes{'files'}{$file}{'num-changes'} += $count;
    }
    $$changes{'num-total-changes'} += $$changes{'files'}{$file}{'num-changes'};
  }
  $Data::Dumper::Indent = 1;
  &make_dir($output_dir);
  my $path = "$output_dir/changes.pl";
  open(my $out, ">", $path) or die "cannot open > '$path': $!";
  print $out &Dumper($changes);
  close($out);

  if (! $ENV{'silent'}) {
    if ($$cmd_info{'opts'}{'summary'} || $ENV{'summary'}) {
      print sort @$lines;
      print "num-files=$$changes{'num-files'}" . $nl;
      print "num-total-changes=$$changes{'num-total-changes'}" . $nl;
      print "num-macros=$num_macros" . $nl;
      print "num-tokens=$num_tokens" . $nl;
      print "match-count=$gbl_match_count" . $nl;
    }
  }
} # start
sub func_info_from_macro_func {
  my ($seq, $func_tbl) = @_;
  die "incorrect argument" if ! $seq || scalar @$seq < 3;
  die "incorrect argument" if ! $func_tbl;
  my ($open_tkn, $func_name) = ($$seq[0], $$seq[1]);
  my $func = $$func_tbl{$open_tkn}{$func_name};
  return ($func_name, $func, []) if scalar @$seq == 3;
  my ($first_arg_i, $last_arg_i) = (2, scalar @$seq -2);
  return ($func_name, $func, [ @$seq[$first_arg_i..$last_arg_i] ]);
} # func_info_from_macro_func
sub rule_match_func { # similiar to code in rhs_from_template()
  my ($sst, $sst_i, $pattern) = @_;
  my ($func_name, $func, $args) = &func_info_from_macro_func($pattern, $pattern_funcs);
  my ($last_i, $lhs_frag) = &$func($sst, $sst_i, @$args);
  return ($last_i, $lhs_frag); # frag_pair (-1, undef) or ... ( <n>, [ { 'tkn' => '...' }, ... ] )
} # rule_match_func
sub rule_match_pattern_var {
  my ($sst, $sst_i, $constraint_name) = @_;
  my $constraint = $$constraints{$constraint_name};
  if (! defined $constraint) {
    die "Could not find implementation for constraint '$constraint_name'";
  }
  # match by constraint
  my ($last_i, $lhs_frag);
  $last_i = &$constraint($sst, $sst_i);
  if ($last_i != -1 ) {
    $lhs_frag = [ @{$$sst{'tokens'}}[$sst_i..$last_i] ];
  }
  return ($last_i, $lhs_frag); # frag_pair (-1, undef) or ... ( <n>, [ { 'tkn' => '...' }, ... ] )
} # rule_match_pattern_var
sub frag_rules_match {
  my ($sst, $sst_i, $rules, $macros, $macro_name, $pattern_name) = @_;
  my ($lhs_frag, $rhs_frag) = (undef, undef);
  my ($last_i, $lhs, $rhs) =
    &rules_match($sst, $sst_i, $rules, $macros, $macro_name, $pattern_name);
  if ($last_i != -1) {
    $lhs_frag = &flatten($lhs);
    $rhs_frag = &flatten($rhs);
  }
  return ($last_i, $lhs_frag, $rhs_frag);
} # frag_rules_match
sub rules_match {
  my ($sst, $sst_i, $rules, $macros, $macro_name, $pattern_name) = @_;

  for (my $rules_i = 0; $rules_i < scalar @$rules; $rules_i++) {
    #printf STDERR "%s: sst-i: %3i: rule-i: %3i: %s\n", $macro_name, $sst_i, $rules_i, $pattern_name;
    my ($last_i, $lhs, $rhs)
      = &rule_match($sst, $sst_i, $rules, $rules_i, $macros, $macro_name);
    if ($last_i != -1) {
      return ($last_i, $lhs, $rhs);
    }
  }
  return (-1, undef, undef);
} # rules_match
sub rule_match {
  my ($sst, $sst_i, $rules, $rules_i, $macros, $macro_name) = @_;
  my $num_tokens = scalar @{$$sst{'tokens'}};
  return (-1, undef, undef) if $sst_i > $num_tokens - @{$$rules[$rules_i]{'pattern'}};
  return (-1, undef, undef) if $$rules[$rules_i]{'disabled'};
  my $pattern =  $$rules[$rules_i]{'pattern'};
  my $template = $$rules[$rules_i]{'template'};
  $gbl_match_count++;
  my ($last_i, $lhs, $rhs) = ($sst_i, [], []);
  my $i = $last_i - $sst_i; # obviously zero
  my $rhs_for_pattern = {};

  for (my $pattern_i = 0; $pattern_i < @$pattern; $pattern_i++) {
    my ($lhs_frag, $rhs_frag) = (undef, undef);
    my $pattern_name = $$pattern[$pattern_i];

    SWITCH: {
      # match by func
      (ref($$pattern[$pattern_i]) eq 'ARRAY') && do { # ?(...) or ?[...] or ?{...}
        $pattern_name = $$pattern[$pattern_i][0];
        # fixfix: should be recursive here to support nested func use in patterns (already works for templates)
        ($last_i, $lhs_frag)
          = &rule_match_func($sst, $sst_i + $i, $$pattern[$pattern_i]);
        $rhs_frag = $lhs_frag;
        last SWITCH;
      };
      # match by pattern var (alias, aux-rule, macro, or var)
      ($pattern_name =~ /^$pattern_var_re$/) && do { # ?<anything>
        if ($pattern_name eq '?...') {
          # aux-rule | macro recursion
          my $rls = $rules;
          ($last_i, $lhs_frag, $rhs_frag)
            = &frag_rules_match($sst, $sst_i + $i, $rls, $macros, $macro_name, $pattern_name); # indirectly recursive
        } elsif ($pattern_name =~ /^\?$id$/) {
          if ($$macros{'defn'}{$macro_name}{'aliases'} && $$macros{'defn'}{$macro_name}{'aliases'}{$pattern_name}) {
            # alias
            my $rls = [ { 'pattern' => $$macros{'defn'}{$macro_name}{'aliases'}{$pattern_name},
                          'template' => [] } ];
            ($last_i, $lhs_frag, $rhs_frag)
              = &frag_rules_match($sst, $sst_i + $i, $rls, $macros, $macro_name, $pattern_name); # indirectly recursive
            $rhs_frag = $lhs_frag;
          } elsif ($$macros{'defn'}{$macro_name}{'aux-rules'}{$pattern_name}) {
            # aux-rule
            my $rls = $$macros{'defn'}{$macro_name}{'aux-rules'}{$pattern_name};
            ($last_i, $lhs_frag, $rhs_frag)
              = &frag_rules_match($sst, $sst_i + $i, $rls, $macros, $macro_name, $pattern_name); # indirectly recursive
          } elsif ($$macros{'defn'}{$pattern_name}) {
            # macro
            my $rls = $$macros{'defn'}{$pattern_name}{'rules'};
            ($last_i, $lhs_frag, $rhs_frag)
              = &frag_rules_match($sst, $sst_i + $i, $rls, $macros, $pattern_name, $pattern_name); # indirectly recursive
          } else {
            # var
            ($last_i, $lhs_frag)
              = &rule_match_pattern_var($sst, $sst_i + $i, $pattern_name);
            $rhs_frag = $lhs_frag;
          }
        } else {
          die "unexpected pattern var '$pattern_name'" . $nl; # use error
        }
        last SWITCH;
      };
      # match by literal
      ($pattern_name !~ /^$pattern_var_re$/) && do { # not ?<anything>
        ($last_i, $lhs_frag) = &literal($sst, $sst_i + $i, $pattern_name); # literal() is not a pattern-variable constraint
        $rhs_frag = $lhs_frag;
        last SWITCH;
      };
      die "unexpected pattern '$pattern_name'"; # coding error
    } # SWITCH:
    last if $last_i == -1;

    die "" if ! defined $lhs_frag;
    die "" if ! defined $rhs_frag;
    die "" if ! scalar @$lhs_frag;
    $$rhs_frag[0]{'lhs-i'} = $pattern_i;
    # rhs_for_pattern shall only contain patterns, not literals
    # this is because some literals in the lhs might be referred to by
    # a ?<n> AND then also referred to by its literal (for example ','
    # which is common to have multiple per frag).
    if ($pattern_name =~ /^$pattern_var_re$/) { # ?<anything>
      if (! exists $$rhs_for_pattern{$pattern_name}) {
        $$rhs_for_pattern{$pattern_name} = [];
      }
      &add_last($$rhs_for_pattern{$pattern_name}, $rhs_frag);
    }
    &add_last($lhs, $lhs_frag);
    $i = $last_i - $sst_i + 1;
  } # for (...)
  if ($last_i != -1) {
    $rhs = &rhs_from_template($template, $lhs, $rhs_for_pattern, $macro_name);
    &frags_remove_keys($lhs, ['lhs-i']);          # hackhack (maybe)
    &frags_remove_keys($rhs, ['lhs-i', 'rhs-i']); # hackhack (maybe)
  } else {
    ($rhs, $lhs) = (undef, undef);
  }
  &dump_rule_match($macro_name, $sst, $sst_i, $last_i, $lhs, $rhs, $pattern, $template);
  return ($last_i, $lhs, $rhs);
} # rule_match
sub rhs_for_pattern {
  my ($rhs_for_pattern, $index_from_tkn, $tkn) = @_;
  my $rhs = $$rhs_for_pattern{$tkn};

  if (! exists $$index_from_tkn{$tkn}) {
    $$index_from_tkn{$tkn} = 0;
  } else {
    if ($$index_from_tkn{$tkn} + 1 < scalar @{$$rhs_for_pattern{$tkn}}) {
      $$index_from_tkn{$tkn}++;
    }
  }
  my $result = $$rhs_for_pattern{$tkn}[$$index_from_tkn{$tkn}];
  return $result;
} # rhs_for_pattern
sub rhs_from_template_core {
  my ($template, $lhs, $rhs_for_pattern, $macro_name, $unused_lhs_i_set, $unused_rhs_i_set) = @_;
  my $rhs = [];
  my $index_from_tkn = {};

  foreach my $tkn (@$template) {
    die "" if ! defined $tkn;
    my $rhs_frag;

    if (ref($tkn) eq 'ARRAY') { # similiar to rule_match_func()
      my ($func_name, $func, $args) = &func_info_from_macro_func($tkn, $template_funcs);
      $args = &rhs_from_template_core($args, $lhs, $rhs_for_pattern, "$macro_name~", $unused_lhs_i_set, $unused_rhs_i_set); # recursive
      $rhs_frag = &$func(@$args);
    } elsif ($tkn =~ /^\?((?:\-|\+)?\d+)$/) {
      my $j = $1;
      die "" if 0 == $j; # ?0 should return entire match

      if ($j > 0) {
        $j--;
      } else { # may have negative value (-1 is last, -2 second to last, ...)
        $j = scalar @$lhs + $j; # adding a negative index
      }
      $rhs_frag = $$lhs[$j];

      if (defined $$lhs[$j][0]{'lhs-i'} && defined $$unused_lhs_i_set{$$lhs[$j][0]{'lhs-i'}}) {
        delete $$unused_lhs_i_set{$$lhs[$j][0]{'lhs-i'}};
      }
    } else {
      $rhs_frag = &rhs_for_pattern($rhs_for_pattern, $index_from_tkn, $tkn);

      # tkn '?...' with no corresponding match indicates end of recursion
      next if $tkn eq '?...' && ! defined $rhs_frag;

      if ($rhs_frag) {
        foreach my $token (@$rhs_frag) {
          if (defined $$token{'lhs-i'} && defined $$unused_lhs_i_set{$$token{'lhs-i'}}) {
            delete $$unused_lhs_i_set{$$token{'lhs-i'}};
          }
        }
      } elsif ($tkn =~ /^$pattern_var_re$/) {
        die "<file>:<line>: error: macro '$macro_name': pattern-var '$tkn' found in template but not in corresponding pattern." . $nl;
      } else {
        # these are literal tokens that may exists only in the template/rhs and may not in the pattern/lhs
        my $rhs_i = scalar @$rhs;
        $$unused_rhs_i_set{$rhs_i} = 1;
        $$unused_rhs_i_set{$rhs_i} = $tkn if $ENV{'adjust_ws_debug'};
        $rhs_frag = [ { 'tkn' => $tkn } ];
      }
    }
    &add_last($rhs, $rhs_frag);
  }
  return $rhs;
} # rhs_from_template_core
sub adjust_leading_ws {
  my ($frag) = @_;
  for (my $i = scalar @$frag - 1; $i > 0; $i--) {
    if (!         $$frag[$i - 1]{'trailing-ws'} &&
       #!         $$frag[$i - 1]{'leading-ws'}  &&
        ! defined $$frag[$i - 1]{'line'}        &&
                  $$frag[$i    ]{'leading-ws'}  &&
          defined $$frag[$i    ]{'line'}) {
      $$frag[$i - 1]{'leading-ws'} .= $$frag[$i]{'leading-ws'};
      $$frag[$i - 1]{'line'} = $$frag[$i]{'line'};
      delete $$frag[$i]{'leading-ws'};
      delete $$frag[$i]{'line'};
    }
  }
  return $frag;
} # adjust_leading_ws
sub adjust_trailing_ws {
  my ($frag) = @_;
  for (my $i = 0; $i < scalar @$frag - 1; $i++) {
    if ($$frag[$i]{'trailing-ws'}) {
      $$frag[$i + 1]{'trailing-ws'} .= $$frag[$i]{'trailing-ws'};
      delete $$frag[$i]{'trailing-ws'};
    }
  }
  return $frag;
} # adjust_trailing_ws
# eventually remove this when a more general purpose version is available
sub adjust_ws_unused_frags_perfect {
  my ($unused_lhs_frag, $unused_rhs_frag) = @_;
  my $pairs = [];

  ### LHS
  my $lhs_tkn_to_tokens = {};
  foreach my $unused_lhs_token (@$unused_lhs_frag) {
    my $tkn = $$unused_lhs_token{'tkn'};
    if (! defined $$lhs_tkn_to_tokens{$tkn}) {
      $$lhs_tkn_to_tokens{$tkn} = [];
    }
    &add_last($$lhs_tkn_to_tokens{$tkn}, $unused_lhs_token);
  }
  #&dumpref("lhs-tkn-to-tokens: ", $lhs_tkn_to_tokens);

  ### RHS
  my $rhs_tkn_to_tokens = {};
  foreach my $unused_rhs_token (@$unused_rhs_frag) {
    my $tkn = $$unused_rhs_token{'tkn'};
    if (! defined $$rhs_tkn_to_tokens{$tkn}) {
      $$rhs_tkn_to_tokens{$tkn} = [];
    }
    &add_last($$rhs_tkn_to_tokens{$tkn}, $unused_rhs_token);
  }
  #&dumpref("rhs-tkn-to-tokens: ", $rhs_tkn_to_tokens);

  foreach my $unused_lhs_token (@$unused_lhs_frag) {
    my $tkn = $$unused_lhs_token{'tkn'};
    if (defined $$lhs_tkn_to_tokens{$tkn} && scalar @{$$lhs_tkn_to_tokens{$tkn}} == 1 &&
        defined $$rhs_tkn_to_tokens{$tkn} && scalar @{$$rhs_tkn_to_tokens{$tkn}} == 1) {
      my $pair = [ $$unused_lhs_token{'lhs-i'}, $$rhs_tkn_to_tokens{$tkn}[0]{'rhs-i'} ];
      &add_last($pairs, $pair);
    }
  }
  # its perfect if and only if the length of $pairs equals the length of $unused_lhs_frag
  $pairs = [] if scalar @$pairs != scalar @$unused_lhs_frag;
  return $pairs;
} # adjust_ws_unused_frags_perfect

# 0: try perfect solution
# 1: try to find corresponding balenced () [] {} in both $unused_lhs_frag and $unused_rhs_frag
# 2: look or exact matches (may have multiple exact; always take first or last?)
# 3:
sub adjust_ws_unused_frags {
  my ($lhs, $rhs, $unused_lhs_frag, $unused_rhs_frag) = @_;
  my $pairs = &adjust_ws_unused_frags_perfect($unused_lhs_frag, $unused_rhs_frag);
  return $pairs if scalar @$pairs;

  foreach my $unused_lhs_token (@$unused_lhs_frag) {
    last if ! scalar @$unused_rhs_frag;
    my $unused_rhs_token = &remove_first($unused_rhs_frag); # very bad algorithm
    my $pair = [ $$unused_lhs_token{'lhs-i'},
                 $$unused_rhs_token{'rhs-i'} ];
    &add_last($pairs, $pair);
  }
  return $pairs;
} # adjust_ws_unused_frags
sub adjust_ws_unused_sets {
  my ($lhs, $rhs, $unused_lhs_i_set, $unused_rhs_i_set) = @_;
  return [] if ! scalar keys %$unused_lhs_i_set || ! scalar keys %$unused_rhs_i_set;
  &dumpref("unused_lhs_i_set:     ", $unused_lhs_i_set) if $ENV{'adjust_ws_debug'};
  &dumpref("unused_rhs_i_set:     ", $unused_rhs_i_set) if $ENV{'adjust_ws_debug'};
  my $unused_lhs_i_seq = [ sort keys %$unused_lhs_i_set ];
  my $unused_rhs_i_seq = [ sort keys %$unused_rhs_i_set ];

  my $unused_lhs_frag = [];
  my $unused_rhs_frag = [];

  foreach my $unused_lhs_i (@$unused_lhs_i_seq) {
    die "" if ! defined $$lhs[$unused_lhs_i][0]{'lhs-i'};
    die "" if ! $$lhs[$unused_lhs_i][0]{'tkn'};
    my $token = { 'lhs-i' => $$lhs[$unused_lhs_i][0]{'lhs-i'},
                  'tkn'   => $$lhs[$unused_lhs_i][0]{'tkn'} };
    die "" if $$token{'lhs-i'} != $unused_lhs_i;
    die "" if $$token{'tkn'} =~ /^$pattern_var_re$/;
    &add_last($unused_lhs_frag, $token);
  }
  foreach my $unused_rhs_i (@$unused_rhs_i_seq) {
    die "" if ! $$rhs[$unused_rhs_i][0]{'tkn'};
    my $token = { 'rhs-i' => $unused_rhs_i,
                  'tkn'   => $$rhs[$unused_rhs_i][0]{'tkn'} };
    die "" if $$token{'tkn'} =~ /^$pattern_var_re$/;
    &add_last($unused_rhs_frag, $token);
  }
  &dumpref("unused-lhs-frag:      ", $unused_lhs_frag) if $ENV{'adjust_ws_debug'};
  &dumpref("unused-rhs-frag:      ", $unused_rhs_frag) if $ENV{'adjust_ws_debug'};

  my $pairs = &adjust_ws_unused_frags($lhs, $rhs, $unused_lhs_frag, $unused_rhs_frag);
  &dumpref("lhs-i-to-rhs-i-pairs: ", $pairs) if $ENV{'adjust_ws_debug'};
  return $pairs;
} # adjust_ws_unused_sets
sub adjust_ws_unused {
  my ($lhs, $rhs, $pairs) = @_;
  &dumpref("rhs-1:                ", $rhs) if $ENV{'adjust_ws_debug'};
  foreach my $pair (@$pairs) {
    my ($unused_lhs_i, $unused_rhs_i) = @$pair;
    my $tkn = $$rhs[$unused_rhs_i][0]{'tkn'};
    %{$$rhs[$unused_rhs_i][0]} = %{$$lhs[$unused_lhs_i][0]};
    $$rhs[$unused_rhs_i][0]{'tkn'} = $tkn;
  }
  &dumpref("rhs-2:                ", $rhs) if $ENV{'adjust_ws_debug'};
  return $rhs;
} # adjust_ws_unused
sub rhs_from_template {
  my ($template, $lhs, $rhs_for_pattern, $macro_name) = @_;
  my $unused_lhs_i_set = {};
  my $unused_rhs_i_set = {};

  foreach my $lhs_frag (@$lhs) {
    foreach my $lhs_token (@$lhs_frag) {
      if (defined $$lhs_token{'lhs-i'}) {
        $$unused_lhs_i_set{$$lhs_token{'lhs-i'}} = 1;
        $$unused_lhs_i_set{$$lhs_token{'lhs-i'}} = $$lhs_token{'tkn'} if $ENV{'adjust_ws_debug'};
      }
    }
  }
  &dumpref("lhs:                  ", $lhs)             if $ENV{'adjust_ws_debug'};
  &dumpref("rhs-for-pattern:      ", $rhs_for_pattern) if $ENV{'adjust_ws_debug'};
  my $rhs = &rhs_from_template_core($template, $lhs, $rhs_for_pattern, $macro_name, $unused_lhs_i_set, $unused_rhs_i_set);

  if (! $ENV{'no_adjust_ws'}) {
    my $pairs = &adjust_ws_unused_sets($lhs, $rhs, $unused_lhs_i_set, $unused_rhs_i_set);
    $rhs = &adjust_ws_unused($lhs, $rhs, $pairs);
  }
  return $rhs
} # rhs_from_template
sub rule_replace {
  my ($sst, $sst_i, $last_i, $lhs, $rhs, $macro_name) = @_;

  my $lhs_frag = &flatten($lhs);
  my $rhs_frag = &flatten($rhs);
  $rhs_frag = &adjust_leading_ws($rhs_frag);

  if (0) {
    &dumpref("lhs:      ", $lhs);
    &dumpref("lhs-frag: ", $lhs_frag);
  }
  if (scalar @$rhs_frag == 0) {
    my $all_ws = '';
    foreach my $token (@$lhs_frag) {
      $all_ws .= $$token{'leading-ws'}  if defined $$token{'leading-ws'};
      $all_ws .= $$token{'trailing-ws'} if defined $$token{'trailing-ws'};
    }
    $all_ws =~ s/\h+(\v+)/$1/g; # trim trailing spaces/tabs
    &sst::add_lost_ws($sst, $sst_i, $all_ws) if $all_ws;
  }
  if (0) {
    &dumpref("rhs:      ", $rhs);
    &dumpref("rhs-frag: ", $rhs_frag);
  }
  my $lhs_num_tokens = $last_i - $sst_i + 1;
  #&assert($lhs_num_tokens - scalar @{&flatten($lhs)});
  my $rhs_frag_num_tokens = scalar @$rhs_frag;
  my $common_num_tokens = 0;
  my $min_num_tokens = &min($lhs_num_tokens, $rhs_frag_num_tokens);

  for (my $j = 0; $j < $min_num_tokens; $j++) {
    if ($$sst{'tokens'}[$sst_i + $j]{'tkn'} eq $$rhs_frag[$j]{'tkn'}) {
      $common_num_tokens++;
    } else {
      last;
    }
  }
  if (1) {
    if ($lhs_num_tokens == $common_num_tokens && $common_num_tokens == $rhs_frag_num_tokens) {
      # the pattern and the template yield are exactly the same
      # it would create an infinite loop
      if($debug >= 1) {
        &sst::warning($sst, $last_i, "possible infinite loop: macro $macro_name \[replacement skipped]");
      }
      return ($common_num_tokens, $lhs_num_tokens, $rhs_frag_num_tokens);
    }
    if ($lhs_num_tokens == $common_num_tokens && $common_num_tokens < $rhs_frag_num_tokens) {
      &sst::warning($sst, $last_i, "possible infinite loop: macro $macro_name");
    }
    if ($common_num_tokens > 0) {
      # adjust the slice smaller here
    }
  }
  my $rhs_num_tokens = &sst::splice($sst, $sst_i, $lhs_num_tokens, $rhs_frag);
  assert($rhs_frag_num_tokens - $rhs_num_tokens);
  return ($common_num_tokens, $lhs_num_tokens, $rhs_num_tokens);
} # rule_replace
sub rules_match_and_replace {
  my ($sst, $sst_i, $macros, $macro_name, $rules) = @_;
  my $change_count = 0;
  return $change_count if $$macros{'defn'}{$macro_name}{'disabled'};
  my ($last_i, $lhs, $rhs)
    = &rules_match($sst, $sst_i, $rules, $macros, $macro_name, "<main>");
  if ($last_i != -1) {
    &dump_rules_match_and_replace($macro_name, $sst, $sst_i, $last_i, $lhs, $rhs);
    my ($common_num_tokens, $lhs_num_tokens, $rhs_num_tokens)
      = &rule_replace($sst, $sst_i, $last_i, $lhs, $rhs, $macro_name);

    if ($ENV{'verbose'}) {
      my $filestr = &sst::filestr($sst);
      #print STDERR &tighten($filestr);
      print STDERR '---' . $nl;
      print STDERR $filestr;

      if ($filestr !~ m/\n$/) {
        print STDERR $nl;
      }
    }
    if ($lhs_num_tokens > $common_num_tokens) {
      if (! defined $$sst{'changes'}{'macros'}{$macro_name}) {
        $$sst{'changes'}{'macros'}{$macro_name} = 0;
      }
      $$sst{'changes'}{'macros'}{$macro_name}++;
      $change_count++;
    }
  }
  return $change_count;
} # rules_match_and_replace
sub macro_expand_core {
  my ($sst, $sst_i, $macros, $macro_name, $expanded_macro_names) = @_;
  my $change_count = 0;

  # currently not used (and it may not be a requirement)
  foreach my $depend_macro_name (@{$$macros{'defn'}{$macro_name}{'before'} ||= [] }) {
    if (! exists($$expanded_macro_names{$depend_macro_name})) {
      $change_count += &macro_expand_core($sst, $sst_i, $macros, $depend_macro_name, $expanded_macro_names); # recursive
      $$expanded_macro_names{$depend_macro_name} = 1;
    }
  }
  $change_count += &rules_match_and_replace($sst, $sst_i, $macros, $macro_name, $$macros{'defn'}{$macro_name}{'rules'});
  return $change_count;
} # macro_expand_core
sub macros_expand_index {
  my ($sst, $sst_i, $macros) = @_;
  my $change_count = 0;

  foreach my $macro_name (@{$$macros{'names'}}) {
    if ($change_count = &macro_expand_core($sst, $sst_i, $macros, $macro_name, {})) {
      last;
    }
  }
  return $change_count;
} # macros_expand_index
sub macros_expand {
  my ($sst, $macros) = @_;
  my $file = $$sst{'file'} if $$sst{'file'};
  #print "macros-expand(\"$file\")" . $nl;

  $debug = 0;
  if ($ENV{'DKT_MACROS_DEBUG'}) { # 0 or 1 or 2 or 3
    $debug = $ENV{'DKT_MACROS_DEBUG'};
  }
  if ($debug) {
    print STDERR "[" . $nl;
  }
  for (my $sst_i = 0; $sst_i < @{$$sst{'tokens'}}; $sst_i++) {
    while (&macros_expand_index($sst, $sst_i, $macros)) {
      # nothing
    }
  }
  if ($debug) {
    print STDERR "]," . $nl;
  }
} # macros_expand
unless (caller) {
  &start(\@ARGV);
}
1;
