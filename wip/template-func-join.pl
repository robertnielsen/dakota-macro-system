#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;

my $nl = "\n";

use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Useqq     = 1;
$Data::Dumper::Sortkeys  = 1;
$Data::Dumper::Indent    = 1;   # default = 2

sub dk_join {
  my ($seqs) = @;
  for (my $i = 0; $i < @$seqs; $i++) {
    my $seq = $$seqs[$i];

  }
}
print &Dumper(&dk_join([ '_' ], [ 'int', 'uint' ] [ '_' ] [ 16, 32, 64 ] [ '_t' ]);
