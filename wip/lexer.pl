#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;
use sort 'stable';

my $nl = "\n";

use Carp; $SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use Data::Dumper;
$Data::Dumper::Terse =     1;
$Data::Dumper::Deepcopy =  1;
$Data::Dumper::Purity =    1;
$Data::Dumper::Useqq =     1;
$Data::Dumper::Sortkeys =  1;
$Data::Dumper::Indent =    1;  # default = 2

my $tbl = {};

while (<>) {
  chomp $_;
  my $chars = [split('', $_)];
  my $cur = $tbl;
  if (scalar @$chars < 2) { next; }
  foreach my $char (@$chars) {
    if (! exists $$cur{$char}) {
      $$cur{$char} = {};
    }
    $cur = $$cur{$char};
  }
}
print &Dumper($tbl);
