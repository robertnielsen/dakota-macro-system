macro:
  list of 'rules' (one or more, ordered)

rule:
  a 'pattern' to 'template' transformation

pattern:
  a list of literals, aliases, pattern variables, & pattern functions
  eventually aux-rules and aux-macros

template:
  a list of literals, alias matches, pattern variable matches, & pattern functions results
  eventually aux-rules and aux-macros

token:
  a table containing at least 'str' => <non-null-non-empty>

fragment:
  a list of tokens

lhs:
  a list of fragments (from a pattern match)

rhs:
  a list of fragments (from a template expansion) used to replace the range represented by lhs
  note: the rhs must be flattened ($frag = &flatten($rhs)) before its spliced into the sst{'tokens'}

---

  { klass ?ident ?block }
  =>
  { KLASS ?ident ?block }

pattern:  [ "klass", "?ident", "?block" ]

template: [ "KLASS", "?ident", "?block" ]

lhs:      [ [{"str" => "klass"}], [{"str" => "some-klass"}], [{"str" => "{"},{"str" => "}"}] ]

rhs:      [ [{"str" => "KLASS"}], [{"str" => "some-klass"}], [{"str" => "{"},{"str" => "}"}] ]

---

  { slots          ?block   } # pattern
  =>
  { struct slots-t ?block ; } # template

pattern:  [ "slots", "?block" ] # 2 tokens

template: [ "struct", "slots-t", "?block", ";" ] # 4 tokens

lhs:      [ [{"str" => "slots"}],  [{"str" => "{"},{"str" => "}"}] ] # 2 fragments

rhs:      [ [{"str" => "struct"}], [{"str" => "slots-t"}], [{"str" => "{"},{"str" => "}"}], [{"str" => ";"}]] # 4 fragments
